/**
 * Created by InspireUI on 19/02/2017.
 */
import React from 'react';
import {View, StatusBar, AsyncStorage} from 'react-native';
import {toast, closeDrawer} from './Omni';
import {Constants, Device, Styles} from "@common";
import { MyToast, MyNetInfo} from "@containers";
import Navigation from "@navigation"

import {applyMiddleware, compose, createStore} from 'redux';
import {persistStore, autoRehydrate} from 'redux-persist';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import Reactotron from 'reactotron-react-native'
import './ReactotronConfig'
const middleware = [thunk];
import reducers from '@redux'

import MenuSide from "@components/LeftMenu/MenuScale";
// import MenuSide from "@components/LeftMenu/MenuOverlay";
// import MenuSide from "@components/LeftMenu/MenuSmall";
// import MenuSide from "@components/LeftMenu/MenuWide";


// const store = createStore(reducers, {}, applyMiddleware(...middleware));

let store = null;
if (__DEV__) {
  store = Reactotron.createStore(reducers, compose(applyMiddleware(...middleware), autoRehydrate()));
}
else {
  store = compose(applyMiddleware(...middleware), autoRehydrate())(createStore)(reducers);
}
persistStore(store, {
  storage: AsyncStorage,
  blacklist: [
    // 'categories', //home screen categories

    // 'layouts'
    // 'user'//TODO: remove this after complete user login
  ]
});

export default class Router extends React.Component {
  goToScreen = (routeName, params, isReset: boolean = false) => {
    const {navigator} = this.refs;
    if (!navigator) {
      return toast('Cannot navigate');
    }

    navigator.dispatch({type: 'Navigation/NAVIGATE', routeName, params});
    closeDrawer();
  }

  componentWillMount() {
    !Device.isIphoneX && StatusBar.setHidden(true);
  }

  render() {
    return <Provider store={store}>
      <MenuSide goToScreen={this.goToScreen}
                routes={<View style={Styles.app}>
                  {Device.isIphoneX && <StatusBar backgroundColor="#000" />}
                  <Navigation ref={'navigator'}/>
                  <MyToast />
                  <MyNetInfo />
                </View>}/>
    </Provider>
  }
}


// const initialState = Navigation.router.getStateForAction(Navigation.router.getActionForPathAndParams('SplashScreen'));
// const navReducer = (state = initialState, action) => {
//   const nextState = Navigation.router.getStateForAction(action, state);
//   return nextState || state;
// };
// const appReducer = combineReducers({
//   ...reducers,
//   nav: navReducer,
// });


// const mapStateToProps = (state) => ({
//   nav: state.nav
// });
// const AppWithNavigationState = connect(mapStateToProps)(Menu);
//
// export default class Root extends React.Component {
//   render() {
//     return (
//       <Provider store={store}>
//         <AppWithNavigationState />
//       </Provider>
//     );
//   }
// }

