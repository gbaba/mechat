'use strict'
import React from 'react'
import {StackNavigator, TabNavigator} from 'react-navigation';

import {Color, Styles, Images} from '@common'
import {TabBar, TabBarIcon} from '@components'
import {SplashScreen} from '@containers';

import HomeScreen from './HomeScreen'


const HomeStack = StackNavigator({
  Home: {screen: HomeScreen},
},)

export default StackNavigator({
    SplashScreen: {screen: SplashScreen},
    HomeScreen: {
      screen: TabNavigator({
          Default: {
            screen: ({navigation}) => <HomeStack screenProps={{rootNavigation: navigation}}/>,
            navigationOptions: {
              header: null,
              tabBarIcon: ({tintColor}) => <TabBarIcon icon={Images.IconHome} tintColor={tintColor}/>,
            }
          },

          CategoriesScreen: {
            screen: ({navigation}) => <HomeScreen screenProps={{rootNavigation: navigation}}/>,
            navigationOptions: {
              header: null,
              tabBarIcon: ({tintColor}) => <TabBarIcon css={{width: 18, height: 18}} icon={Images.IconCategory}
                                                       tintColor={tintColor}/>
            }
          },

          //DetailScreen: {screen: DetailScreen},
          Search: {
            screen: ({navigation}) => <HomeScreen screenProps={{rootNavigation: navigation}}/>,
            navigationOptions: {
              header: null,
              tabBarIcon: ({tintColor}) => <TabBarIcon css={{width: 18, height: 18}} icon={Images.IconSearch}
                                                       tintColor={tintColor}/>
            }
          },
          CartScreen: {screen: HomeScreen},
          MyOrders: {screen: HomeScreen},
          WishListScreen: {screen: HomeScreen},
          SettingScreen: {screen: HomeScreen},
          Detail: {screen: HomeScreen, navigationOptions: {tabBarVisible: false}},
        },
        {
          tabBarComponent: TabBar,
          tabBarPosition: 'bottom',
          swipeEnabled: false,
          animationEnabled: false,
          tabBarOptions: {
            showIcon: true,
            showLabel: true,
            activeTintColor: Color.tabbarTint,
            inactiveTintColor: Color.tabbarColor,
            // style: {
            //   backgroundColor: Color.tabbar,
            // },
          },
          lazy: false
        })
    },
  },
  {
    navigationOptions: {
      headerTintColor: Color.headerTintColor,
      headerStyle: Styles.Common.toolbar,
      headerTitleStyle: Styles.Common.headerStyle,
    }
  }
)
