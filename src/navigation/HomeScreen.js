import React, {Component} from "react";
import {Logo, Menu, HeaderHomeRight} from './IconNav'

import {Color, Images, Styles} from '@common'
import {TabBarIcon} from '@components'
import {Home} from "@containers";
import {warn} from "@app/Omni"

export default class HomeScreen extends Component {
  static navigationOptions = ({navigation}) => ({
    title: Logo(),
    headerLeft: Menu(),
    headerRight: HeaderHomeRight(navigation),
    tabBarIcon: ({tintColor}) => <TabBarIcon icon={Images.IconHome} tintColor={tintColor}/>,

    headerTintColor: Color.headerTintColor,
    headerStyle: Styles.Common.toolbar,
    headerTitleStyle: Styles.Common.headerStyle,
  })

  render() {

    // const {rootNavigation} = this.props.screenProps;

    return <Home  />
  }
}