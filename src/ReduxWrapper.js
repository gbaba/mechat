/**
 * Created by InspireUI on 18/02/2017.
 */
'use strict';
import React, {Component} from 'react';
import {AsyncStorage, I18nManager} from 'react-native';
import {Provider} from 'react-redux';
import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import {persistStore, autoRehydrate} from 'redux-persist';
import thunk from 'redux-thunk';
// import logger from 'redux-logger';
import {Languages, Constants} from '@common';
import Reactotron from 'reactotron-react-native'
import './ReactotronConfig';

import Router from './Router';

const middleware = [
  thunk,
  //more middleware
];

// You have to import every reducers and combine them.

import {reducer as UserRedux} from '@redux/UserRedux';
import {reducer as ToastReducer} from '@redux/ToastRedux';
import {reducer as LangRedux} from '@redux/LangRedux';
import {reducer as NetInfoReducer} from '@redux/NetInfoRedux';

// ... more reducers

const reducers = combineReducers({

  user: UserRedux,
  toast: ToastReducer,
  netInfo: NetInfoReducer,
  language: LangRedux,

});


// const store = createStore(reducers, {}, applyMiddleware(...middleware));


var store = null;
if (__DEV__) {
  store = Reactotron.createStore(reducers, compose(applyMiddleware(...middleware), autoRehydrate({log: true})));
}
else {
  store = compose(applyMiddleware(...middleware), autoRehydrate())(createStore)(reducers);
}


// const store = compose(applyMiddleware(...middleware), autoRehydrate())(createStore)(reducers);
persistStore(store, {
  storage: AsyncStorage,
  blacklist: [
    // 'categories', //home screen categories

    // 'user'//TODO: remove this after complete user login
  ],
}, () => {
  //set default Language for App
  Languages.setLanguage(store.getState().language.lang)
  // Enable for mode RTL
  I18nManager.forceRTL(store.getState().language.rtl);
});

export default class ReduxWrapper extends Component {
  state = {appIsReady: false};

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    console.ignoredYellowBox = ['Warning: View.propTypes', 'Warning: BackAndroid'];
  }

  componentWillUnmount() {
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onRegistered(notifData) {
    console.log("Device had been registered for push notifications!", notifData);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  render() {
    return <Provider store={store}>
      <Router />
    </Provider>
  }
}
