/**
 * Created by InspireUI on 22/02/2017.
 */

import WooCommerceAPI from './WooCommerceAPI';
import {Constants, Styles} from "@common";
import {error, warn, log} from '@app/Omni';

const API = new WooCommerceAPI({
    url: Constants.WooCommerce.url,
    consumerKey: Constants.WooCommerce.consumerKey,
    consumerSecret: Constants.WooCommerce.consumerSecret,
    wp_api: Constants.WooCommerce.wp_api,
    version: Constants.WooCommerce.version,
    timeout: Constants.WooCommerce.timeout
});

const WooWorker = {
  getCategories: async () => {
    try {
      const response = await API.get('products/categories', {
        hide_empty: true,
        per_page: 100,
        order: 'desc',
        orderby: 'count'
      });
      return await response.json();
    } catch (err) {
      error(err);
    }
  },
  getCustomerByEmail: async (email) => {
    try {
      const response = await API.get('customers', {email});
      return await response.json();
    } catch (err) {
      error(err);
    }
  },
  getCustomerById: async (id) => {
    try {
      const response = await API.get('customers/' + id);
      return await response.json();
    } catch (err) {
      error(err);
    }
  },
  productsByCategoryId: async (category, per_page, page) => {
    try {
      const response = await API.get('products', {
        category, per_page, page,
        purchasable: true,

      });
      log({category, per_page, page});
      return await response.json();
    } catch (err) {
      error(err);
    }
  },

  productsByCategoryTag: async (category, tag, per_page, page) => {
    try {
      let params = {per_page, page, purchasable: true}

      if (category != '') {
        params = {...params, category}
      }
      else {
        params = {...params, tag}
      }

      const response = await API.get('products', params);

      return await response.json();
    } catch (err) {
      error(err);
    }
  },


  reviewsByProductId: async (id) => {
    try {
      const response = await API.get('products/' + id + '/reviews')
      return await response.json();
    } catch (err) {
      error(err);
    }
  },
  createOrder: async (data) => {
    try {
      const response = await API.post('orders', data)
      return await response.json();
    } catch (e) {
      console.log(e);
    }
  },
  productsByTagId: async (tagId, per_page, page) => {
    try {
      const response = await API.get('products', {
        tag: tagId,
        per_page,
        page,
      });
      return await response.json();
    } catch (err) {
      error(err);
    }
  },
  productsByName: async (name, per_page, page) => {
    try {
      const response = await API.get('products', {
        search: name,
        per_page,
        page
      });
      return await response.json();
    } catch (err) {
      error(err);
    }
  },
  productSticky: async (per_page, page) => {
    try {
      const response = await API.get('products', {
        tag: Constants.tagIdBanner,
        per_page,
        page,
      });
      return await response.json();
    } catch (err) {
      error(err);
    }
  },
  getAllProducts: async (per_page, page) => {
    try {
      let data = {
        per_page,
        page,
        order: Constants.PostList.order,
        orderby: Constants.PostList.orderby,
      }
      const response = await API.get('products', data);
      return await response.json();
    } catch (err) {
      error(err);
    }
  },
  ordersByCustomerId: async (id, per_page, page) => {
    try {
      let data = {
        customer: id,
        per_page,
        page,
      }
      const response = await API.get('orders', data);
      return await response.json();
    } catch (err) {
      error(err);
    }
  },
  createNewOrder: (data, callback) => {
    API.post('orders', data)
      .then((response) => response.json())
      .then((json) => {
        if (json.code === undefined)
          callback(json)
        else {
          // console.log(JSON.stringify(json))
        }
      }).catch((error) => console.log(error))
  },
  setOrderStatus: (orderId, status, callback) => {
    API.post('orders/' + orderId, {status: status}).then((json) => {
      if (json.code === undefined)
        callback(json)
      else {
        alert(JSON.stringify(json.code))
        // console.log(JSON.stringify(json))
      }
    }).catch((error) => console.log(error))
  },
  productVariant: async (product, per_page, page) => {
    try {
      let data = {
        per_page,
        page,
      }
      const response = await API.get('products/' + product.id + '/variations', data);
      return await response.json();
    }
    catch (err) {
      error(err);
    }
  },
  getProductRelated: async (product) => {
    try {
      let data = {
        include: [product]
      }
      const response = await API.get('products', data);
      return await response.json();
    }
    catch (err) {
      error([err]);
    }
  },
};
export default WooWorker;
