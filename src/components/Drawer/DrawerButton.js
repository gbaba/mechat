/**
 * Created by InspireUI on 27/02/2017.
 */

import React, {Component, PropTypes} from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';

import {Styles, Color, Constants} from "@common";
import {Icon} from '@app/Omni';

class DrawerButton extends Component {

  render() {
    const {text, onPress, icon} = this.props;
    return (
      <TouchableOpacity
        style={[styles.container, Constants.RTL && {flexDirection: 'row-reverse'}]}
        onPress={onPress}>
        <Icon name={icon} color={Color.lightTextPrimary} size={20} />
        <Text style={[styles.text, Constants.RTL && {marginRight: 20}]}>{text}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...Styles.Common.RowCenterLeft,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  text: {
    marginLeft: 20,
    color: Color.lightTextPrimary,
    fontSize: Styles.FontSize.medium,
    fontFamily: Constants.fontFamily
  },
});


DrawerButton.defaultProps = {
  text: 'Default button name',
  onPress: () => alert('Drawer button clicked'),
};

export default DrawerButton;
