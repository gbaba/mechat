import {combineReducers} from 'redux';

import {reducer as UserRedux} from './UserRedux';
import {reducer as ToastReducer} from './ToastRedux';
import {reducer as LangRedux} from './LangRedux';
import {reducer as NetInfoReducer} from './NetInfoRedux';

export default combineReducers({

  user: UserRedux,
    toast: ToastReducer,
  netInfo: NetInfoReducer,
  language: LangRedux,
  
});