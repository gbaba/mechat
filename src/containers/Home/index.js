
import React, {Component, PropTypes} from 'react';
import {View, Text} from 'react-native'
import {connect} from 'react-redux';
import {Languages, Constants} from '@common';
import {toast, warn} from "@app/Omni";


class Home extends Component {
  render() {
    return (<View style={{flex: 1, paddingTop: 32, backgroundColor: '#fff'}}>
     		<Text> Home Screen</Text>
    </View>)
  }
}

const mapStateToProps = ({user}) => ({user});

export default connect(mapStateToProps)(Home);
