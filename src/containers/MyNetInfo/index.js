/**
 * Created by InspireUI on 28/02/2017.
 */

import React, {PropTypes} from 'react';
import {View, Text, StyleSheet, NetInfo} from 'react-native';
import {connect} from 'react-redux';

import {Color,Styles} from "@common";
import {toast} from '@app/Omni';

class MyNetInfo extends React.Component {
    constructor(props) {
        super(props);

        this.skipFirstToast = true;
    }

    componentDidMount() {
        NetInfo.isConnected.addEventListener('change', this._handleConnectionChange);
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('change', this._handleConnectionChange);
    }

    _handleConnectionChange = (isConnected) => {
        this.props.updateConnectionStatus(isConnected);
        if (!isConnected) return;

        if (!this.skipFirstToast) {
            toast('Regain internet connection');
        } else {
            this.skipFirstToast = false;
        }
    };

    render() {
        const {netInfo} = this.props;

        if (netInfo.isConnected) return <View/>;
        return (
            <View style={styles.connectionStatus}>
                <Text style={styles.connectionText}>{'NO CONNECTION'}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    connectionStatus: {
        position: 'absolute', bottom: 0,
        width: Styles.width,
        backgroundColor: Color.error,
        padding: 10,
        alignItems: 'center'
    },
    connectionText: {
        color: 'white',
        fontWeight: 'bold',
    }
});


const mapStateToProps = (state) => {
    return {
        netInfo: state.netInfo,
    };
};

const mapDispatchToProps = (dispatch) => {
    const {actions} = require('./../../redux/NetInfoRedux');

    return {
        updateConnectionStatus: (isConnected) => dispatch(actions.updateConnectionStatus(isConnected)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyNetInfo);
